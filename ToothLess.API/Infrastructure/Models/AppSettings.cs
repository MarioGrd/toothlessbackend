﻿namespace ToothLess.API.Infrastructure.Models
{
    using ToothLess.Infrastructure.Base.Database;

    public class AppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }
}
