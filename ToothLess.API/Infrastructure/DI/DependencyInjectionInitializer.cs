﻿namespace ToothLess.API.Infrastructure.DI
{
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using FluentValidation;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using ToothLess.API.Infrastructure.Pipeline;
    using ToothLess.API.Infrastructure.Validation;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Database;
    using ToothLess.Infrastructure.Repositories.DomainRepository;
    using ToothLess.Infrastructure.Repositories.QueryRepository;
    using ToothLess.Infrastructure.Services.Implementation;

    public class DependencyInjectionInitializer
    {
        public static IServiceProvider Initialize(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUnitOfWork, ToothLessContext>();

            services.AddMediatR(typeof(ValidatorBehavior<,>).Assembly);

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));

            var builder = new ContainerBuilder();

            builder.Populate(services);

            builder
                .RegisterGeneric(typeof(QueryRepository<>))
                .As(typeof(IQueryRepository<>))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<ValidatorFactory>()
                .As<IValidatorFactory>();

            builder
                .RegisterAssemblyTypes((typeof(ValidatorFactory).Assembly))
                .AsClosedTypesOf(typeof(IValidator<>));

            RegisterDomainRepositories(builder);
            RegisterImplementationServices(builder);

            return builder.Build().Resolve<IServiceProvider>();
        }

        /// <summary>
        /// Registers strongly typed domain repositories.
        /// </summary>
        /// <param name="builder">Autofac container builder</param>
        public static void RegisterDomainRepositories(ContainerBuilder builder)
        {
            builder
                .RegisterType<AppointmentDomainRepository>()
                .As(typeof(IAppointmentDomainRepository))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<ClinicDomainRepository>()
                .As(typeof(IClinicDomainRepository))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<PatientDomainRepository>()
                .As(typeof(IPatientDomainRepository))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Registers implementation services
        /// </summary>
        /// <param name="builder">Autofac container builder</param>
        public static void RegisterImplementationServices(ContainerBuilder builder)
        {
            builder
                .RegisterType<TimeService>()
                .As(typeof(ITimeService))
                .InstancePerLifetimeScope();
        }
    }
}
