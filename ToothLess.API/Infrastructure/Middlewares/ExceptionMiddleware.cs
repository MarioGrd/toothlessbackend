﻿namespace ToothLess.API.Infrastructure.Middlewares
{
    using FluentValidation;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Newtonsoft.Json;
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Helpers;

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            ArgumentChecker.CheckNotNull(new { next });

            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this._next(context);
            }
            catch(Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            if (exception is DomainException)
            {
                    var result = JsonConvert.SerializeObject(new { error = exception.Message });
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    return context.Response.WriteAsync(result);

            }
            else if (exception is ValidationException validationException)
            {
                var dictionary = new ModelStateDictionary();

                foreach (var error in validationException.Errors)
                    dictionary.AddModelError(error.PropertyName, error.ErrorMessage);

                var result = JsonConvert.SerializeObject(dictionary);
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return context.Response.WriteAsync(result);

            }
            else
            {
                var result = JsonConvert.SerializeObject(new { error = "An unexpected error occured." });
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return context.Response.WriteAsync(result);
            }

        }
    }

    public static class ExceptionMiddlewareExtension
    {
        public static IApplicationBuilder UseExceptionMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
