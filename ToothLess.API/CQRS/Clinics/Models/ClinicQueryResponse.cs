﻿namespace ToothLess.API.CQRS.Clinics.Models
{
    using System;
    using System.Collections.Generic;

    public class ClinicQueryResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Doctor { get; set; }
        public List<ClinicProcedureQueryResponse> Procedures { get; set; }
        public ClinicWorkTimeQueryResponse WorkTime { get; set; }
    }
}
