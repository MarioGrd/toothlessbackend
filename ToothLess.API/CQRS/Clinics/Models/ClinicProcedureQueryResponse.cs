﻿namespace ToothLess.API.CQRS.Clinics.Models
{
    using System;

    public class ClinicProcedureQueryResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Minutes { get; set; }
        public int Type { get; set; }
    }
}
