﻿namespace ToothLess.API.CQRS.Clinics.Models
{
    public class ClinicWorkTimeQueryResponse
    {
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
    }
}
