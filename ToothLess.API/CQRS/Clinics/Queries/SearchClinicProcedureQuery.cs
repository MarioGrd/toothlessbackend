﻿namespace ToothLess.API.CQRS.Clinics.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Clinics.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class SearchClinicProcedureQuery : IRequest<List<ClinicProcedureQueryResponse>>
    {
        public string Name { get; set; }
        public Guid ClinicId { get; set; }
    }

    public class SearchClinicProcedureQueryValidator : AbstractValidator<SearchClinicProcedureQuery>
    {
        public SearchClinicProcedureQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.ClinicId)
                .Must(id => id != null)
                .WithMessage($"{nameof(SearchClinicProcedureQuery.ClinicId)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");
        }
    }

    public class SearchClinicProcedureQueryHandler : IRequestHandler<SearchClinicProcedureQuery, List<ClinicProcedureQueryResponse>>
    {
        private readonly IQueryRepository<ClinicProcedure> _procedures;

        public SearchClinicProcedureQueryHandler(IQueryRepository<ClinicProcedure> procedures)
        {
            ArgumentChecker.CheckNotNull(new { procedures });

            this._procedures = procedures;
        }

        public async Task<List<ClinicProcedureQueryResponse>> Handle(SearchClinicProcedureQuery request, CancellationToken token)
        {
            var query =
                await this._procedures.Query
                    .Where(cp => cp.ClinicId == request.ClinicId)
                    .Where(cp => cp.Name.StartsWith(request.Name))
                    .Select(cp => new ClinicProcedureQueryResponse()
                    {
                        Id = cp.Id,
                        Name = cp.Name,
                        Price = cp.Price,
                        Type = cp.ProcedureTypeId,
                        Minutes = cp.Minutes
                    })
                    .OrderBy(ob => ob.Name)
                    .Skip(0)
                    .Take(10)
                    .ToListAsync();

            return query;
        }
    }
}
