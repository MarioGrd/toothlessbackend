﻿namespace ToothLess.API.CQRS.Clinics.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Clinics.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class GetClinicQuery : IRequest<ClinicQueryResponse>
    {
        public Guid Id { get; set; }

        public GetClinicQuery(Guid id)
        {
            this.Id = id;
        }
    }

    public class GetClinicQueryValidator : AbstractValidator<GetClinicQuery>
    {
        public GetClinicQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.Id)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetClinicQuery.Id)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");
        }
    }

    public class GetClinicQueryHandler : IRequestHandler<GetClinicQuery, ClinicQueryResponse>
    {
        private readonly IQueryRepository<Clinic> _clinics;

        public GetClinicQueryHandler(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this._clinics = clinics;
        }

        public async Task<ClinicQueryResponse> Handle(GetClinicQuery request, CancellationToken token)
        {
            var query =
                await this._clinics.Query
                .Include(p => p.Procedures)
                .Where(c => c.Id == request.Id)
                .Select(c => new ClinicQueryResponse()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Doctor = $"{c.Doctor.FirstName} {c.Doctor.LastName}",
                    Procedures = c.Procedures.Select(cp => new ClinicProcedureQueryResponse() {
                        Id = cp.Id,
                        Name = cp.Name,
                        Price = cp.Price,
                        Type = cp.ProcedureTypeId,
                        Minutes = cp.Minutes

                    }).ToList(),
                    WorkTime = new ClinicWorkTimeQueryResponse()
                    {
                        StartHour = c.WorkTime.StartHour,
                        StartMinute = c.WorkTime.StartMinute,
                        EndHour = c.WorkTime.EndHour,
                        EndMinute = c.WorkTime.EndMinute
                    }
                })
                .SingleOrDefaultAsync();

            return query;
        }
    }
}
