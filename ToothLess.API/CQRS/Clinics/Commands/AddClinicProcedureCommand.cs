﻿namespace ToothLess.API.CQRS.Clinics.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Clinics.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class AddClinicProcedureCommand : IRequest<ClinicProcedureQueryResponse>
    {
        public Guid ClinicId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int TypeId { get; set; }

        public AddClinicProcedureCommand(Guid clinicId, string name, double price, int typeId)
        {
            this.ClinicId = clinicId;
            this.Name = name;
            this.Price = price;
            this.TypeId = typeId;
        }
    }

    public class AddClinicProcedureCommandValidator : AbstractValidator<AddClinicProcedureCommand>
    {
        public AddClinicProcedureCommandValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(p => p.ClinicId)
                .Must(clinicId => clinicId != null)
                .WithMessage($"'{nameof(AddClinicProcedureCommand.ClinicId)}' is required.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");

            this.RuleFor(p => p.Name)
                .Must(name => !string.IsNullOrWhiteSpace(name))
                .WithMessage($"'{nameof(AddClinicProcedureCommand.Name)}' is required.")
                .MinimumLength(ClinicInvariantConstants.ProcedureNameMinLength)
                .WithMessage($"'{nameof(AddClinicProcedureCommand.Name)}' must be greater than '{ClinicInvariantConstants.ProcedureNameMinLength}'")
                .MaximumLength(ClinicInvariantConstants.ProcedureNameMaxLength)
                .WithMessage($"'{nameof(AddClinicProcedureCommand.Name)}' must be lower than '{ClinicInvariantConstants.ProcedureNameMaxLength}'");

            this.RuleFor(p => p.Price)
                .GreaterThan(0)
                .WithMessage($"'{nameof(AddClinicProcedureCommand.Price)}' must be greater than 0.");

            this.RuleFor(p => p.TypeId)
                .Must(type => ProcedureType.List().Any(p => p.Id == type))
                .WithMessage($"'{nameof(AddClinicProcedureCommand.TypeId)}' is required.");
        }
    }

    public class AddClinicProcedureCommandHandler : IRequestHandler<AddClinicProcedureCommand, ClinicProcedureQueryResponse>
    {
        private readonly IClinicDomainRepository _clinicRepository;

        public AddClinicProcedureCommandHandler(IClinicDomainRepository clinicRepository)
        {
            ArgumentChecker.CheckNotNull(new { clinicRepository });

            this._clinicRepository = clinicRepository;
        }
        public async Task<ClinicProcedureQueryResponse> Handle(AddClinicProcedureCommand request, CancellationToken cancellationToken)
        {
            var clinic = await this._clinicRepository.GetByIdAsync(request.ClinicId);

            var procedure = clinic.AddProcedure(name: request.Name, price: request.Price, typeId: request.TypeId);

            await _clinicRepository.UnitOfWork.SaveChangesAsync();

            var query = new ClinicProcedureQueryResponse()
            {
                Id = procedure.Id,
                Name = procedure.Name,
                Minutes = procedure.Minutes,
                Price = procedure.ProcedureTypeId,
                Type = procedure.ProcedureTypeId
            };

            return query;
        }
    }
}
