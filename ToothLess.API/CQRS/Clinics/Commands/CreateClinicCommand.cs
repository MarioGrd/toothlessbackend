﻿namespace ToothLess.API.CQRS.Clinics.Commands
{
    using FluentValidation;
    using MediatR;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;
    using ToothLess.Infrastructure.Base.Helpers;

    public class CreateClinicCommand : IRequest
    {
        public string Name { get; set; }

        public int StartHour { get; set; }
        public int StartMinute { get; set; }

        public int EndHour { get; set; }
        public int EndMinute { get; set; }

        public string DoctorFirstName { get; set; }
        public string DoctorLastName { get; set; }

        public CreateClinicCommand(string name, int startHour, int startMinute, int endHour, int endMinute, string doctorFirstName, string doctorLastName)
        {
            this.Name = name;
            this.StartHour = startHour;
            this.StartMinute = startMinute;
            this.EndHour = endHour;
            this.EndMinute = endMinute;
            this.DoctorFirstName = doctorFirstName;
            this.DoctorLastName = doctorLastName;
        }
    }

    public class CreateClinicCommandValidator : AbstractValidator<CreateClinicCommand>
    {
        public CreateClinicCommandValidator()
        {
            this.RuleFor(p => p.Name)
                .Must(name => !string.IsNullOrWhiteSpace(name))
                .WithMessage($"'{nameof(CreateClinicCommand.Name)}' is required.")
                .MinimumLength(ClinicInvariantConstants.ClinicNameMinLength)
                .WithMessage($"'{nameof(CreateClinicCommand.Name)}' must be greater than '{ClinicInvariantConstants.ClinicNameMinLength}'")
                .MaximumLength(ClinicInvariantConstants.ClinicNameMaxLength)
                .WithMessage($"'{nameof(CreateClinicCommand.Name)}' must be lower than '{ClinicInvariantConstants.ClinicNameMaxLength}'");

            this.RuleFor(p => p.StartHour)
                .GreaterThanOrEqualTo(ClinicInvariantConstants.HoursMin)
                .WithMessage($"'{nameof(CreateClinicCommand.StartHour)}' must be greater or equal to {ClinicInvariantConstants.HoursMin}.")
                .LessThanOrEqualTo(ClinicInvariantConstants.HoursMax)
                .WithMessage($"'{nameof(CreateClinicCommand.StartHour)}' must be less or equal to {ClinicInvariantConstants.HoursMax}.");

            this.RuleFor(p => p.StartMinute)
                .GreaterThanOrEqualTo(ClinicInvariantConstants.MinutesMin)
                .WithMessage($"'{nameof(CreateClinicCommand.StartMinute)}' must be greater or equal to {ClinicInvariantConstants.MinutesMin}.")
                .LessThanOrEqualTo(ClinicInvariantConstants.MinutesMax)
                .WithMessage($"'{nameof(CreateClinicCommand.StartMinute)}' must be less or equal to {ClinicInvariantConstants.MinutesMax}.");

            this.RuleFor(p => p.EndHour)
                .GreaterThanOrEqualTo(ClinicInvariantConstants.HoursMin)
                .WithMessage($"'{nameof(CreateClinicCommand.EndHour)}' must be greater or equal to {ClinicInvariantConstants.HoursMin}.")
                .LessThanOrEqualTo(ClinicInvariantConstants.HoursMax)
                .WithMessage($"'{nameof(CreateClinicCommand.EndHour)}' must be less or equal to {ClinicInvariantConstants.HoursMax}.");

            this.RuleFor(p => p.EndMinute)
                .GreaterThanOrEqualTo(ClinicInvariantConstants.MinutesMin)
                .WithMessage($"'{nameof(CreateClinicCommand.EndMinute)}' must be greater or equal to {ClinicInvariantConstants.MinutesMin}.")
                .LessThanOrEqualTo(ClinicInvariantConstants.MinutesMax)
                .WithMessage($"'{nameof(CreateClinicCommand.EndMinute)}' must be less or equal to {ClinicInvariantConstants.MinutesMax}.");

            this.RuleFor(p => p.DoctorFirstName)
                .Must(name => !string.IsNullOrWhiteSpace(name))
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorFirstName)}' is required.")
                .MinimumLength(ClinicInvariantConstants.DoctorNameMinLength)
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorFirstName)}' must be greater than '{ClinicInvariantConstants.DoctorNameMinLength}'")
                .MaximumLength(ClinicInvariantConstants.DoctorNameMaxLength)
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorFirstName)}' must be lower than '{ClinicInvariantConstants.DoctorNameMaxLength}'");

            this.RuleFor(p => p.DoctorLastName)
                .Must(name => !string.IsNullOrWhiteSpace(name))
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorLastName)}' is required.")
                .MinimumLength(ClinicInvariantConstants.DoctorNameMinLength)
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorLastName)}' must be greater than '{ClinicInvariantConstants.DoctorNameMinLength}'")
                .MaximumLength(ClinicInvariantConstants.DoctorNameMaxLength)
                .WithMessage($"'{nameof(CreateClinicCommand.DoctorLastName)}' must be lower than '{ClinicInvariantConstants.DoctorNameMaxLength}'");

        }
    }

    public class CreateClinicCommandHandler : AsyncRequestHandler<CreateClinicCommand>
    {
        private readonly IClinicDomainRepository _clinicRepository;

        public CreateClinicCommandHandler(IClinicDomainRepository clinicRepository)
        {
            ArgumentChecker.CheckNotNull(new { clinicRepository });

            this._clinicRepository = clinicRepository;
        }

        protected override async Task Handle(CreateClinicCommand request, CancellationToken cancellationToken)
        {
            var workTime = new WorkTime(
                startMinute: request.StartMinute,
                startHour: request.StartHour,
                endMinute: request.EndMinute,
                endHour: request.EndHour);

            var doctor = new ClinicDoctor(
                firstName: request.DoctorFirstName,
                lastName: request.DoctorLastName);


            var clinic = new Clinic(name: request.Name, doctor: doctor, workTime: workTime);

            _clinicRepository.Add(clinic);
            await _clinicRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
