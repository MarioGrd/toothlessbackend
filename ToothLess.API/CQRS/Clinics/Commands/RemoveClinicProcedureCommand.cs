﻿namespace ToothLess.API.CQRS.Clinics.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class RemoveClinicProcedureCommand : IRequest
    {
        public Guid ClinicId { get; set; }
        public Guid ProcedureId { get; set; }

        public RemoveClinicProcedureCommand(Guid clinicId, Guid procedureId)
        {
            this.ClinicId = clinicId;
            this.ProcedureId = procedureId;
        }
    }

    public class RemoveClinicProcedureCommandValidator : AbstractValidator<RemoveClinicProcedureCommand>
    {
        public RemoveClinicProcedureCommandValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(p => p.ClinicId)
                .Must(clinicId => clinicId != null)
                .WithMessage($"'{nameof(RemoveClinicProcedureCommand.ClinicId)}' is required.")
                .MustAsync(async (clinicId, token) => await clinics.Query.AnyAsync(s => s.Id == clinicId))
                .WithMessage($"Clinic not found.");

            this.RuleFor(p => p.ProcedureId)
                .Must(procedureId => procedureId != null)
                .WithMessage($"'{nameof(RemoveClinicProcedureCommand.ProcedureId)}' is required.");
        }
    }

    public class RemoveClinicProcedureCommandHandler : AsyncRequestHandler<RemoveClinicProcedureCommand>
    {
        private readonly IClinicDomainRepository _clinicRepository;

        public RemoveClinicProcedureCommandHandler(IClinicDomainRepository clinicRepository)
        {
            ArgumentChecker.CheckNotNull(new { clinicRepository });

            this._clinicRepository = clinicRepository;
        }

        protected override async Task Handle(RemoveClinicProcedureCommand request, CancellationToken cancellationToken)
        {
            var clinic = await this._clinicRepository.GetByIdAsync(request.ClinicId);

            clinic.RemoveProcedure(request.ProcedureId);

            await _clinicRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
