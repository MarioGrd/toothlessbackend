﻿namespace ToothLess.API.CQRS.Reports.Models
{
    using System;

    public class ReportItemQueryResponse
    {
        public Guid? Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Price { get; set; }
        public int? AppointmentStatusId { get; set; }
    }
}
