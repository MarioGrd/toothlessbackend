﻿namespace ToothLess.API.CQRS.Reports.Models
{
    using System.Collections.Generic;

    public class ReportQueryResponse
    {
        public ReportDataQueryResponse Data { get; set; }
        public List<ReportItemQueryResponse> Items { get; set; }

        public ReportQueryResponse(ReportDataQueryResponse data, List<ReportItemQueryResponse> items)
        {
            this.Data = data;
            this.Items = items;
        }
    }
}
