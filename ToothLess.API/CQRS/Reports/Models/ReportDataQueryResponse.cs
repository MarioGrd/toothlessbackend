﻿namespace ToothLess.API.CQRS.Reports.Models
{
    public class ReportDataQueryResponse
    {
        public int MissedCount { get; set; }
        public double MissedTotalTime { get; set; }

        public int CompletedCount { get; set; }
        public double CompletedTotalTime { get; set; }

        public int UnusedCount { get; set; }
        public double UnusedTotalTime { get; set; }

        public ReportDataQueryResponse(int missedCount, double missedTotalTime, int completedCount, double completedTotalTime, int unusedCount, double unusedTotalTime)
        {
            this.MissedCount = missedCount;
            this.MissedTotalTime = missedTotalTime;
            this.CompletedCount = completedCount;
            this.CompletedTotalTime = completedTotalTime;
            this.UnusedCount = unusedCount;
            this.UnusedTotalTime = unusedTotalTime;
        }
    }
}
