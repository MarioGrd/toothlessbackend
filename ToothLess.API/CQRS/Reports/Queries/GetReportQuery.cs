﻿namespace ToothLess.API.CQRS.Reports.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Reports.Models;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.AppointmentAggregate.Enumerations;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Doman.Helpers;
    using ToothLess.Infrastructure.Base.Filters;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;


    public class GetReportQuery : TimeIntervalFilter, IRequest<ReportQueryResponse>
    {
        public Guid ClinicId { get; set; }
        public Guid? ProcedureId { get; set; }
    }

    public class GetReportQueryValidator : AbstractValidator<GetReportQuery>
    {
        public GetReportQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.ClinicId)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetReportQuery.ClinicId)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");
        }
    }

    public class GetReportQueryHandler : IRequestHandler<GetReportQuery, ReportQueryResponse>
    {
        private readonly IQueryRepository<Appointment> _appointments;
        private readonly IQueryRepository<ClinicProcedure> _procedures;
        private readonly IQueryRepository<Clinic> _clinic;

        public GetReportQueryHandler(IQueryRepository<Appointment> appointment, IQueryRepository<ClinicProcedure> procedures, IQueryRepository<Clinic> clinic)
        {
            ArgumentChecker.CheckNotNull(new { appointment, procedures, clinic });

            this._appointments = appointment;
            this._procedures = procedures;
            this._clinic = clinic;
        }

        public async Task<ReportQueryResponse> Handle(GetReportQuery request, CancellationToken token)
        {
            var appointmentQuery =
                this._appointments.Query
                    .Where(a =>
                        a.ClinicId == request.ClinicId &&
                        a.AppointmentStatusId != AppointmentStatus.CREATED.Id &&
                        a.StartTime > request.StartTime &&
                        a.EndTime < request.EndTime)
                    .WhereIf(request.ProcedureId != null, p => p.ProcedureId == request.ProcedureId);

            var procedureQuery = this._procedures.Query;

            var clinic = await this._clinic.Query.SingleOrDefaultAsync(c => c.Id == request.ClinicId);

            var query =
                await appointmentQuery
                    .Join(
                        procedureQuery,
                        appointment => appointment.ProcedureId,
                        procedure => procedure.Id,
                        (appointment, procedure) => new ReportItemQueryResponse()
                        {
                            Id = appointment.Id,
                            Price = procedure.Price,
                            StartDate = appointment.StartTime,
                            EndDate = appointment.EndTime,
                            AppointmentStatusId = appointment.AppointmentStatusId
                        })
                    .OrderBy(ob => ob.StartDate)
                    .GroupBy(gb => gb.StartDate.Date)
                    .ToListAsync();

            List<ReportItemQueryResponse> unusedReports = new List<ReportItemQueryResponse>();

            var time = DateTimeHelper.DateRange(request.StartTime, request.EndTime).ToList();

            foreach (var t in time)
            {
                var iterationDayItems = query.Where(p => p.Key.Date == t).SelectMany(p => p).ToList();

                var clientStartTime = new DateTime(t.Year, t.Month, t.Day, clinic.WorkTime.StartHour, clinic.WorkTime.StartMinute, 0);
                var clientEndTime = new DateTime(t.Year, t.Month, t.Day, clinic.WorkTime.EndHour, clinic.WorkTime.EndMinute, 0);

                if (iterationDayItems.Count() == 0)
                {
                    var appointment = new ReportItemQueryResponse()
                    {
                        StartDate = clientStartTime,
                        EndDate = clientEndTime,
                        AppointmentStatusId = 1,
                        Id = null,
                        Price = 0
                    };

                    unusedReports.Add(appointment);
                }
                else
                {
                    for (var x = 0; x < iterationDayItems.Count(); x++)
                    {
                        if (x == 0 && iterationDayItems[x].StartDate > clientStartTime)
                        {
                            var appointment = new ReportItemQueryResponse()
                            {
                                StartDate = clientStartTime,
                                EndDate = iterationDayItems[x].StartDate,
                                AppointmentStatusId = 1,
                                Id = null,
                                Price = 0
                            };

                            unusedReports.Add(appointment);
                        }

                        if (x == iterationDayItems.Count() - 1 && iterationDayItems[x].EndDate <= clientEndTime)
                        {
                            var appointment = new ReportItemQueryResponse()
                            {
                                StartDate = iterationDayItems[x].EndDate,
                                EndDate = clientEndTime,
                                AppointmentStatusId = 1,
                                Id = null,
                                Price = 0
                            };

                            unusedReports.Add(appointment);
                        }
                        else
                        {
                            var appointment = new ReportItemQueryResponse()
                            {
                                Id = null,
                                StartDate = iterationDayItems[x].EndDate,
                                EndDate = iterationDayItems[x + 1].StartDate,
                                Price = 0,
                                AppointmentStatusId = 1,
                            };

                            unusedReports.Add(appointment);
                        }
                    }
                }
            }

            var unusedHours =
                unusedReports
                    .Sum(p => p.EndDate.Subtract(p.StartDate)
                    .TotalMinutes) / 60;

            var unusedCount =
                unusedReports
                    .Count();

            var usedReports =
                query.SelectMany(p => p.Select(x => x).ToList());

            var completedHours =
                usedReports
                    .Where(p => p.AppointmentStatusId == AppointmentStatus.COMPLETED.Id)
                    .Sum(p => p.EndDate.Subtract(p.StartDate)
                    .TotalMinutes) / 60;

            var completedCount =
                usedReports
                    .Count();

            var missedHours =
                usedReports
                    .Where(p => p.AppointmentStatusId == AppointmentStatus.MISSED.Id)
                    .Sum(p => p.EndDate.Subtract(p.StartDate)
                    .TotalMinutes) / 60;

            var missedCount =
                usedReports
                    .Where(p => p.AppointmentStatusId == AppointmentStatus.MISSED.Id)
                    .Count();

            List<ReportItemQueryResponse> reports = new List<ReportItemQueryResponse>();

            reports.AddRange(unusedReports);
            reports.AddRange(usedReports);

            var items = reports.OrderBy(ob => ob.StartDate).ToList();
            var data = new ReportDataQueryResponse(
                missedCount: missedCount,
                missedTotalTime: missedHours,
                completedCount: completedCount,
                completedTotalTime: completedHours,
                unusedCount: unusedCount,
                unusedTotalTime: unusedHours);


            return new ReportQueryResponse(data: data, items: items);
        }
    }
}
