﻿namespace ToothLess.API.CQRS.Appointments.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Appointments.Models;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Infrastructure.Base.Filters;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class GetClinicAppointmentsQuery : TimeIntervalFilter, IRequest<List<AppointmentQueryResponse>>
    {
        public Guid ClinicId { get; set; }
    }

    public class GetClinicAppointmentsQueryValidator : AbstractValidator<GetClinicAppointmentsQuery>
    {
        public GetClinicAppointmentsQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.ClinicId)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetClinicAppointmentsQuery.ClinicId)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");

            this.RuleFor(q => q.StartTime)
                .Must(time => time != null)
                .WithMessage($"{nameof(GetClinicAppointmentsQuery.StartTime)} cannot be null.");

            this.RuleFor(q => q.EndTime)
                .Must(time => time != null)
                .WithMessage($"{nameof(GetClinicAppointmentsQuery.EndTime)} cannot be null.");
        }
    }

    public class GetClinicAppointmentsQueryHandler : IRequestHandler<GetClinicAppointmentsQuery, List<AppointmentQueryResponse>>
    {
        private readonly IQueryRepository<Appointment> _appointments;
        private readonly IQueryRepository<Patient> _patients;


        public GetClinicAppointmentsQueryHandler(IQueryRepository<Appointment> appointments, IQueryRepository<Patient> patients)
        {
            ArgumentChecker.CheckNotNull(new { appointments, patients });

            this._appointments = appointments;
            this._patients = patients;
        }

        public async Task<List<AppointmentQueryResponse>> Handle(GetClinicAppointmentsQuery request, CancellationToken token)
        {
            var appointmentQuery =
                this._appointments.Query
                    .Where(a => a.ClinicId == request.ClinicId && 
                    (a.StartTime >= request.StartTime && a.EndTime <= request.EndTime));

            var patientQuery =
                this._patients.Query;

            var query =
                await appointmentQuery
                        .Join(
                            patientQuery,
                            appointment => appointment.PatientId,
                            patient => patient.Id,
                            (appointment, patient) => new AppointmentQueryResponse()
                            {
                                Id = appointment.Id,
                                StartDate = appointment.StartTime,
                                EndDate = appointment.EndTime,
                                AppointmentStatusId = appointment.AppointmentStatusId,
                                Patient = patient.GetFullName()
                            })
                        .ToListAsync();

            return query;
        }
    }
}
