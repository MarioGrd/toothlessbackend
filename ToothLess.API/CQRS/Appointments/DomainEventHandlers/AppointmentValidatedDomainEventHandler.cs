﻿namespace ToothLess.API.CQRS.Appointments.DomainEventHandlers
{
    using MediatR;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.DomainEvents;
    using ToothLess.Infrastructure.Base.Helpers;

    public class AppointmentValidatedDomainEventHandler : INotificationHandler<AppointmentValidatedDomainEvent>
    {
        private readonly IAppointmentDomainRepository _appointmentRepository;

        public AppointmentValidatedDomainEventHandler(IAppointmentDomainRepository appointmentRepository)
        {
            ArgumentChecker.CheckNotNull(new { appointmentRepository });

            this._appointmentRepository = appointmentRepository;
        }

        public async Task Handle(AppointmentValidatedDomainEvent notification, CancellationToken cancellationToken)
        {
            var appointment = new Appointment(
                appointmentId: notification.AppointmentId,
                procedureId: notification.ProcedureId,
                startTime: notification.StartTime,
                endTime: notification.EndTime,
                clinicId: notification.ClinicId,
                patientId: notification.PatientId);

            this._appointmentRepository.Add(appointment);

            await this._appointmentRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
