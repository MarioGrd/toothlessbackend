﻿namespace ToothLess.API.CQRS.Appointments.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class MarkAppointmentCompletedCommand : IRequest
    {
        public Guid AppointmentId { get; set; }

        public MarkAppointmentCompletedCommand(Guid appointmentId)
        {
            this.AppointmentId = appointmentId;
        }
    }

    public class MarkAppointmentCompletedCommandValidator : AbstractValidator<MarkAppointmentCompletedCommand>
    {
        public MarkAppointmentCompletedCommandValidator(IQueryRepository<Appointment> appointments)
        {
            ArgumentChecker.CheckNotNull(new { appointments });

            this.RuleFor(p => p.AppointmentId)
                .Must(appointmentId => appointmentId != null)
                .WithMessage($"'{nameof(MarkAppointmentCompletedCommand.AppointmentId)}' is required.")
                .MustAsync(async (id, token) => await appointments.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Appointment not found.");
        }
    }

    public class MarkAppointmentCompletedCommandHandler : AsyncRequestHandler<MarkAppointmentCompletedCommand>
    {
        private readonly IAppointmentDomainRepository _appointmentRepository;

        public MarkAppointmentCompletedCommandHandler(IAppointmentDomainRepository appointmentRepository)
        {
            ArgumentChecker.CheckNotNull(new { appointmentRepository });

            this._appointmentRepository = appointmentRepository;
        }

        protected override async Task Handle(MarkAppointmentCompletedCommand request, CancellationToken cancellationToken)
        {
            var appointment = await this._appointmentRepository.GetByIdAsync(request.AppointmentId);

            appointment.MarkAppointmentCompleted();

            await this._appointmentRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
