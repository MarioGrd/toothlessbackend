﻿namespace ToothLess.API.CQRS.Appointments.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class MarkAppointmentMissedCommand : IRequest
    {
        public Guid AppointmentId { get; set; }

        public MarkAppointmentMissedCommand(Guid appointmentId)
        {
            this.AppointmentId = appointmentId;
        }
    }

    public class MarkAppointmentMissedCommandValidator : AbstractValidator<MarkAppointmentMissedCommand>
    {
        public MarkAppointmentMissedCommandValidator(IQueryRepository<Appointment> appointments)
        {
            ArgumentChecker.CheckNotNull(new { appointments });

            this.RuleFor(p => p.AppointmentId)
                .Must(appointmentId => appointmentId != null)
                .WithMessage($"'{nameof(MarkAppointmentMissedCommand.AppointmentId)}' is required.")
                .MustAsync(async (id, token) => await appointments.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Appointment not found.");
        }
    }

    public class MarkAppointmentMissedCommandHandler : AsyncRequestHandler<MarkAppointmentMissedCommand>
    {
        private readonly IAppointmentDomainRepository _appointmentRepository;

        public MarkAppointmentMissedCommandHandler(IAppointmentDomainRepository appointmentRepository)
        {
            ArgumentChecker.CheckNotNull(new { appointmentRepository });

            this._appointmentRepository = appointmentRepository;
        }

        protected override async Task Handle(MarkAppointmentMissedCommand request, CancellationToken cancellationToken)
        {
            var appointment = await this._appointmentRepository.GetByIdAsync(request.AppointmentId);

            appointment.MarkAppointmentMissed();

            await this._appointmentRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
