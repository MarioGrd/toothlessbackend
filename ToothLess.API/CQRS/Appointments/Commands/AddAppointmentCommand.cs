﻿namespace ToothLess.API.CQRS.Appointments.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Appointments.Models;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class AddAppointmentCommand : IRequest<AppointmentQueryResponse>
    {
        public Guid ClinicId { get; set; }
        public Guid PatientId { get; set; }
        public Guid ProcedureId { get; set; }

        public DateTime StartTime { get; set; }

        public AddAppointmentCommand(Guid clinicId, Guid patientId, Guid procedureId, DateTime startTime)
        {
            this.ClinicId = clinicId;
            this.PatientId = patientId;
            this.ProcedureId = procedureId;
            this.StartTime = startTime;
        }
    }

    public class AddAppointmentCommandValidator : AbstractValidator<AddAppointmentCommand>
    {
        public AddAppointmentCommandValidator(IQueryRepository<Clinic> clinics, IQueryRepository<Patient> patients, IQueryRepository<ClinicProcedure> procedures)
        {
            ArgumentChecker.CheckNotNull(new { clinics, patients, procedures });

            this.RuleFor(p => p.ClinicId)
                .Must(id => id != null)
                .WithMessage($"'{nameof(AddAppointmentCommand.ClinicId)}' is required.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");

            this.RuleFor(p => p.PatientId)
                .Must(id => id != null)
                .WithMessage($"'{nameof(AddAppointmentCommand.PatientId)}' is required.")
                .MustAsync(async (id, token) => await patients.Query.AnyAsync(s => s.Id == id));

            this.RuleFor(p => p.StartTime)
                .Must(time => time != null)
                .WithMessage($"'{nameof(AddAppointmentCommand.StartTime)}' is required.");
        }
    }

    public class AddAppointmentCommandHandler : IRequestHandler<AddAppointmentCommand, AppointmentQueryResponse>
    {
        private readonly IClinicDomainRepository _clinicRepository;
        private readonly IQueryRepository<Appointment> _appointments;
        private readonly IQueryRepository<Patient> _patients;

        public AddAppointmentCommandHandler(IClinicDomainRepository clinicRepository, IQueryRepository<Appointment> appointments, IQueryRepository<Patient> patients)
        {
            ArgumentChecker.CheckNotNull(new { clinicRepository, appointments, patients });

            this._clinicRepository = clinicRepository;
            this._appointments = appointments;
            this._patients = patients;
        }

        public async Task<AppointmentQueryResponse> Handle(AddAppointmentCommand request, CancellationToken cancellationToken)
        {
            var clinic = await this._clinicRepository.GetByIdAsync(request.ClinicId);

            var appointmentId = Guid.NewGuid();

            clinic.AddAppointmentValidatedDomainEvent(
                appointmentId: appointmentId,
                startTime: request.StartTime,
                patientId: request.PatientId,
                procedureId: request.ProcedureId);

            await this._clinicRepository.UnitOfWork.SaveEntitiesAsync();

            var appointmentQuery = this._appointments.Query.Where(a => a.Id == appointmentId);

            var patientQuery =
                this._patients.Query;

            var query =
                await appointmentQuery
                        .Join(
                            patientQuery,
                            appointment => appointment.PatientId,
                            patient => patient.Id,
                            (appointment, patient) => new AppointmentQueryResponse()
                            {
                                Id = appointment.Id,
                                StartDate = appointment.StartTime,
                                EndDate = appointment.EndTime,
                                AppointmentStatusId = appointment.AppointmentStatusId,
                                Patient = patient.GetFullName()
                            })
                        .SingleOrDefaultAsync();

            return query;
        }
    }
}
