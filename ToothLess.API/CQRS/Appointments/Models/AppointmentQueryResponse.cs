﻿namespace ToothLess.API.CQRS.Appointments.Models
{
    using System;

    public class AppointmentQueryResponse
    {
        public Guid Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int AppointmentStatusId { get; set; }
        public string Patient { get; set; }
    }
}
