﻿namespace ToothLess.API.CQRS.Patients.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class RemoveClinicPatientCommand : IRequest
    {
        public Guid PatientId { get; set; }

        public RemoveClinicPatientCommand(Guid patientId)
        {
            this.PatientId = patientId;
        }
    }

    public class RemoveClinicPatientCommandValidator : AbstractValidator<RemoveClinicPatientCommand>
    {
        public RemoveClinicPatientCommandValidator(IQueryRepository<Patient> patients)
        {
            ArgumentChecker.CheckNotNull(new { patients });

            this.RuleFor(p => p.PatientId)
                .Must(patientId => patientId != null)
                .WithMessage($"'{nameof(RemoveClinicPatientCommand.PatientId)}' is required.")
                .MustAsync(async (id, token) => await patients.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Patient not found.");
        }
    }

    public class RemoveClinicPatientCommandHandler : AsyncRequestHandler<RemoveClinicPatientCommand>
    {
        private readonly IPatientDomainRepository _patientRepository;

        public RemoveClinicPatientCommandHandler(IPatientDomainRepository patientRepository)
        {
            ArgumentChecker.CheckNotNull(new { patientRepository });

            this._patientRepository = patientRepository;
        }

        protected override async Task Handle(RemoveClinicPatientCommand request, CancellationToken cancellationToken)
        {
            var patient = await  this._patientRepository.GetByIdAsync(request.PatientId);

            this._patientRepository.Delete(patient);

            await this._patientRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
