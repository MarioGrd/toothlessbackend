﻿namespace ToothLess.API.CQRS.Patients.Commands
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Patients.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate.Constants;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class AddClinicPatientCommand : IRequest<PatientQueryResponse>
    {
        public Guid ClinicId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string TelephoneNumber { get; set; }
        public DateTime BirthDate { get; set; }

        public AddClinicPatientCommand(Guid clinicId, string firstName, string lastName, string address, string telephoneNumber, DateTime birthDate)
        {
            this.ClinicId = clinicId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.TelephoneNumber = telephoneNumber;
            this.BirthDate = birthDate;
        }
    }

    public class AddClinicPatientCommandValidator : AbstractValidator<AddClinicPatientCommand>
    {
        public AddClinicPatientCommandValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(p => p.ClinicId)
                .Must(clinicId => clinicId != null)
                .WithMessage($"'{nameof(AddClinicPatientCommand.ClinicId)}' is required.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");

            this.RuleFor(p => p.FirstName)
                .Must(firstName => !string.IsNullOrWhiteSpace(firstName))
                .WithMessage($"'{nameof(AddClinicPatientCommand.FirstName)}' is required.")
                .MinimumLength(PatientInvariantConstants.FirstNameMinLength)
                .WithMessage($"'{nameof(AddClinicPatientCommand.FirstName)}' must be greater than '{PatientInvariantConstants.FirstNameMinLength}'")
                .MaximumLength(PatientInvariantConstants.FirstNameMaxLength)
                .WithMessage($"'{nameof(AddClinicPatientCommand.FirstName)}' must be lower than '{PatientInvariantConstants.FirstNameMaxLength}'");

            this.RuleFor(p => p.LastName)
                .Must(lastName => !string.IsNullOrWhiteSpace(lastName))
                .WithMessage($"'{nameof(AddClinicPatientCommand.LastName)}' is required.")
                .MinimumLength(PatientInvariantConstants.LastNameMinLength)
                .WithMessage($"'{nameof(AddClinicPatientCommand.LastName)}' must be greater than '{PatientInvariantConstants.LastNameMinLength}'")
                .MaximumLength(PatientInvariantConstants.LastNameMaxLength)
                .WithMessage($"'{nameof(AddClinicPatientCommand.LastName)}' must be lower than '{PatientInvariantConstants.LastNameMaxLength}'");

            this.RuleFor(p => p.BirthDate)
                .Must(birthDate => birthDate != null)
                .WithMessage($"'{nameof(AddClinicPatientCommand.BirthDate)}' is required.");

            this.RuleFor(p => p.TelephoneNumber)
                .Must(phone => phone != null)
                .WithMessage($"'{nameof(AddClinicPatientCommand.TelephoneNumber)}' is required.");
        }
    }

    public class AddClinicPatientCommandHandler : IRequestHandler<AddClinicPatientCommand, PatientQueryResponse>
    {
        private readonly IPatientDomainRepository _patientRepository;

        public AddClinicPatientCommandHandler(IPatientDomainRepository patientRepository)
        {
            ArgumentChecker.CheckNotNull(new { patientRepository });

            this._patientRepository = patientRepository;
        }

        public async Task<PatientQueryResponse> Handle(AddClinicPatientCommand request, CancellationToken cancellationToken)
        {
            var patient = new Patient(
                firstName: request.FirstName,
                lastName: request.LastName,
                address: request.Address,
                telephoneNumber: request.TelephoneNumber,
                birthDate: request.BirthDate,
                clinicId: request.ClinicId);

            this._patientRepository.Add(patient);

            await this._patientRepository.UnitOfWork.SaveChangesAsync();

            var query = new PatientQueryResponse()
            {
                Id = patient.Id,
                FullName = $"{patient.FirstName} {patient.LastName}",
                TelephoneNumber = patient.TelephoneNumber
            };

            return query;
        }
    }
}
