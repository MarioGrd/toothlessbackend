﻿namespace ToothLess.API.CQRS.Patients.Models
{
    using System;

    public class PatientQueryResponse
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string TelephoneNumber { get; set; }
    }
}
