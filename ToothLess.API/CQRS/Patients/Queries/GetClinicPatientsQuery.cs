﻿namespace ToothLess.API.CQRS.Patients.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Patients.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Infrastructure.Base.Filters;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Base.Pagination;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class GetClinicPatientsQuery : PaginationFilter, IRequest<Paged<PatientQueryResponse>>
    {
        public Guid ClinicId { get; set; }

        public GetClinicPatientsQuery()
        {
        }

        public GetClinicPatientsQuery(int pageNumber, int pageSize, Guid clinicId) : base(pageNumber, pageSize)
        {
            this.ClinicId = clinicId;
        }
    }

    public class GetClinicPatientsQueryValidator : AbstractValidator<GetClinicPatientsQuery>
    {
        public GetClinicPatientsQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.ClinicId)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetClinicPatientsQuery.ClinicId)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");
        }
    }

    public class GetClinicPatientsQueryHandler : IRequestHandler<GetClinicPatientsQuery, Paged<PatientQueryResponse>>
    {
        private readonly IQueryRepository<Patient> _patients;

        public GetClinicPatientsQueryHandler(IQueryRepository<Patient> patients)
        {
            ArgumentChecker.CheckNotNull(new { patients });

            this._patients = patients;
        }

        public async Task<Paged<PatientQueryResponse>> Handle(GetClinicPatientsQuery request, CancellationToken token)
        {
            var query =
                this._patients.Query
                .Where(c => c.ClinicId == request.ClinicId)
                .OrderBy(ob => ob.BirthDate)
                .Select(c => new PatientQueryResponse()
                {
                    Id = c.Id,
                    FullName = c.GetFullName(),
                    TelephoneNumber = c.TelephoneNumber
                });

            var count = await query.CountAsync();

            var data = 
                await query
                .Skip(request.Skip())
                .Take(request.PageSize)
                .ToListAsync();

            return new Paged<PatientQueryResponse>(data, request.PageNumber, request.PageSize, count);
        }
    }
}
