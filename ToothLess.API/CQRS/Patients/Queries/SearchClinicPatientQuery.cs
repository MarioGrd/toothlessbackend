﻿namespace ToothLess.API.CQRS.Patients.Queries
{
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.API.CQRS.Patients.Models;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Infrastructure.Base.Helpers;
    using ToothLess.Infrastructure.Repositories.QueryRepository;

    public class SearchClinicPatientQuery : IRequest<List<PatientQueryResponse>>
    {
        public Guid ClinicId { get; set; }
        public string LastName { get; set; }
    }

    public class SearchClinicPatientQueryValidator : AbstractValidator<SearchClinicPatientQuery>
    {
        public SearchClinicPatientQueryValidator(IQueryRepository<Clinic> clinics)
        {
            ArgumentChecker.CheckNotNull(new { clinics });

            this.RuleFor(q => q.ClinicId)
                .Must(id => id != null)
                .WithMessage($"{nameof(SearchClinicPatientQuery.ClinicId)} cannot be null.")
                .MustAsync(async (id, token) => await clinics.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Clinic not found.");
        }
    }

    public class SearchClinicPatientQueryHandler : IRequestHandler<SearchClinicPatientQuery, List<PatientQueryResponse>>
    {
        private readonly IQueryRepository<Patient> _patients;

        public SearchClinicPatientQueryHandler(IQueryRepository<Patient> patients)
        {
            ArgumentChecker.CheckNotNull(new { patients });

            this._patients = patients;
        }

        public async Task<List<PatientQueryResponse>> Handle(SearchClinicPatientQuery request, CancellationToken token)
        {
            var query =
                await this._patients.Query
                .Where(c => c.ClinicId == request.ClinicId)
                .Where(c => c.LastName.StartsWith(request.LastName))
                .Select(c => new PatientQueryResponse()
                {
                    Id = c.Id,
                    FullName = c.GetFullName(),
                    TelephoneNumber = c.TelephoneNumber
                })
                .OrderBy(ob => ob.FullName)
                .Skip(0)
                .Take(10)
                .ToListAsync();

            return query;
        }
    }
}
