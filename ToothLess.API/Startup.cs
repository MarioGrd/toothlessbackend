﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using ToothLess.API.Infrastructure.DI;
using ToothLess.API.Infrastructure.Middlewares;
using ToothLess.API.Infrastructure.Models;
using ToothLess.Infrastructure.Base.Extenders;

namespace ToothLess.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            // Get settings from json file.
            services.Configure<AppSettings>(this.Configuration);
            var settings = this.Configuration.Get<AppSettings>();

            // Configure DbContext.
            services.AddToothLessDbContext(settings.ConnectionStrings.Main);

            // Configure MVC.
            services.AddMvc(config =>
            {
                config.RespectBrowserAcceptHeader = true;
            });

            // Configure swagger.
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "ToothLess API",
                    Description = "ToothLess API",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Mario Grd", Email = "grd.mario@gmail.com" }
                });

                var applicationPath = PlatformServices.Default.Application.ApplicationBasePath;
                config.IncludeXmlComments(Path.Combine(applicationPath, "ToothLess.API.xml"));
            });

            services.AddAutoMapper();
            services.AddCors();

            return DependencyInjectionInitializer.Initialize(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }

            app.UseCors(config =>
                config
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());

            app.UseExceptionMiddleware();
            app.UseMvcWithDefaultRoute();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ToothLess API");
            });
        }
    }
}
