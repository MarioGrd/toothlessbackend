﻿namespace ToothLess.API.Controllers
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using ToothLess.API.Controllers.Base;
    using ToothLess.API.CQRS.Patients.Commands;
    using ToothLess.API.CQRS.Patients.Queries;

    public class PatientController : WebApiControllerBase
    {
        public PatientController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetClinicPatientsQuery query)
        {
            if (query == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(query);
        }


        [HttpGet("search")]
        public async Task<IActionResult> SearchClinicPatients([FromQuery] SearchClinicPatientQuery query)
        {
            if (query == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(query);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddClinicPatientCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(command);
        }

        [HttpDelete]
        public async Task Delete([FromBody] RemoveClinicPatientCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            await this.ProcessAsync(command);
        }
    }
}
