﻿namespace ToothLess.API.Controllers
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;
    using ToothLess.API.Controllers.Base;
    using ToothLess.API.CQRS.Appointments.Commands;
    using ToothLess.API.CQRS.Appointments.Queries;
    using ToothLess.Infrastructure.Services.Implementation;

    public class AppointmentController : WebApiControllerBase
    {
        public AppointmentController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetClinicAppointmentsQuery query)
        {
            if (query == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(query);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddAppointmentCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(command);
        }

        [HttpPost("{appointmentId}/completed")]
        public async Task Completed(Guid appointmentId)
        {
            var command = new MarkAppointmentCompletedCommand(appointmentId: appointmentId);

            await this.ProcessAsync(command);
        }

        [HttpPost("{appointmentId}/missed")]
        public async Task Missed(Guid appointmentId)
        {
            var command = new MarkAppointmentMissedCommand(appointmentId: appointmentId);

            await this.ProcessAsync(command);
        }

        [HttpDelete]
        public async Task Delete([FromBody] DeleteAppointmentCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            await this.ProcessAsync(command);
        }
    }
}
