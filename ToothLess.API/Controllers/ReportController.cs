﻿namespace ToothLess.API.Controllers
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using ToothLess.API.Controllers.Base;
    using ToothLess.API.CQRS.Reports.Queries;

    public class ReportController : WebApiControllerBase
    {
        public ReportController(IMediator mediator) : base(mediator) { }

        [HttpGet()]
        public async Task<IActionResult> GetReport([FromQuery] GetReportQuery query)
        {
            if (query == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(query);
        }
    }
}
