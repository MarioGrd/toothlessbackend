﻿namespace ToothLess.API.Controllers
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;
    using ToothLess.API.Controllers.Base;
    using ToothLess.API.CQRS.Clinics.Commands;
    using ToothLess.API.CQRS.Clinics.Queries;

    public class ClinicController : WebApiControllerBase
    {
        public ClinicController(IMediator mediator) : base(mediator) { }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var query = new GetClinicQuery(id);

            return await this.ProcessAsync(query);
        }

        [HttpPost]
        public async Task Post([FromBody] CreateClinicCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            await this.ProcessAsync(command);
        }

        [HttpGet("procedure")]
        public async Task<IActionResult> SearchProcedure([FromQuery] SearchClinicProcedureQuery query)
        {
            if (query == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(query);
        }

        [HttpPost("procedure")]
        public async Task<IActionResult> AddProcedure([FromBody] AddClinicProcedureCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            return await this.ProcessAsync(command);
        }

        [HttpDelete("procedure")]
        public async Task DeleteProcedure([FromBody] RemoveClinicProcedureCommand command)
        {
            if (command == null)
            {
                this.BadRequest();
            }

            await this.ProcessAsync(command);
        }
    }
}
