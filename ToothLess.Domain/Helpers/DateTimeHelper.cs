﻿namespace ToothLess.Doman.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class DateTimeHelper
    {
        public static bool IsWeekend(DateTime time) => (time.DayOfWeek == DayOfWeek.Saturday || time.DayOfWeek == DayOfWeek.Sunday) ? true : false;

        public static int TotalMinutes(DateTime time) => (time.Hour * 60 + time.Minute);

        public static IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }
    }
}
