﻿namespace ToothLess.Domain.Aggregates.AppointmentAggregate
{
    using System;
    using ToothLess.Domain.Aggregates.AppointmentAggregate.Enumerations;
    using ToothLess.Domain.Aggregates.AppointmentAggregate.Exceptions;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Domain.Base;

    public class Appointment : Entity, IAggregateRoot
    {
        /// <summary>
        /// Start time.
        /// </summary>
        public DateTime StartTime { get; private set; }

        /// <summary>
        /// End time.
        /// </summary>
        public DateTime EndTime { get; private set; }

        /// <summary>
        /// Clinic identifier.
        /// </summary>
        public Guid ClinicId { get; private set; }

        /// <summary>
        /// Patient identifier.
        /// </summary>
        public Guid PatientId { get; private set; }

        /// <summary>
        /// Procedure identifier.
        /// </summary>
        public Guid ProcedureId { get; private set; }

        /// <summary>
        /// Appointment status identity.
        /// </summary>
        public int AppointmentStatusId { get; private set; }

        /// <summary>
        /// Protected EF constructor.
        /// </summary>
        protected Appointment() { }

        public Appointment(DateTime startTime, DateTime endTime, Guid clinicId, Guid patientId, Guid appointmentId, Guid procedureId)
        {
            if (startTime > endTime || startTime.Day != endTime.Day)
            {
                throw new AppointmentDomainException("Invalid time.");
            }

            if (clinicId == null)
            {
                throw new AppointmentDomainException("Clinic Id not provided.");
            }

            if (patientId == null)
            {
                throw new AppointmentDomainException("Patient Id not provided.");
            }

            this.Id = appointmentId;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.ClinicId = clinicId;
            this.PatientId = patientId;
            this.ProcedureId = procedureId;
            this.AppointmentStatusId = AppointmentStatus.CREATED.Id;
        }

        public void MarkAppointmentCompleted()
        {
            this.AppointmentStatusId = AppointmentStatus.COMPLETED.Id;
        }

        public void MarkAppointmentMissed()
        {
            this.AppointmentStatusId = AppointmentStatus.MISSED.Id;
        }

    }
}
