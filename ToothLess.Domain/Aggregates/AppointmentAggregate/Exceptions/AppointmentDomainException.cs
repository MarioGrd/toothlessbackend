﻿namespace ToothLess.Domain.Aggregates.AppointmentAggregate.Exceptions
{
    using ToothLess.Domain.Base;

    public class AppointmentDomainException : DomainException
    {
        public AppointmentDomainException(string message) : base(message)
        {
        }
    }
}
