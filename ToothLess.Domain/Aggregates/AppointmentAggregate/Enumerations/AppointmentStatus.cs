﻿namespace ToothLess.Domain.Aggregates.AppointmentAggregate.Enumerations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ToothLess.Domain.Aggregates.AppointmentAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class AppointmentStatus : Enumeration
    {
        public static AppointmentStatus CREATED = new AppointmentStatus(1, nameof(CREATED).ToLowerInvariant());
        public static AppointmentStatus COMPLETED = new AppointmentStatus(2, nameof(COMPLETED).ToLowerInvariant());
        public static AppointmentStatus MISSED = new AppointmentStatus(3, nameof(MISSED).ToLowerInvariant());

        public int TimeInMinutes { get; private set; }

        protected AppointmentStatus()
        {
        }

        protected AppointmentStatus(int id, string name)
            : base(id, name)
        {
        }

        public static IEnumerable<AppointmentStatus> List() =>
            new[] { CREATED, COMPLETED, MISSED };

        public static AppointmentStatus FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new AppointmentDomainException($"Possible values for AppointmentStatus: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static AppointmentStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new AppointmentDomainException($"Possible values for AppointmentStatus: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
