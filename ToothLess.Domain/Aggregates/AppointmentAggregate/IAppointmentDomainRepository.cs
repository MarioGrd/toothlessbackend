﻿using ToothLess.Domain.Base;

namespace ToothLess.Domain.Aggregates.AppointmentAggregate
{
    public interface IAppointmentDomainRepository : IDomainRepository<Appointment>
    {
    }
}
