﻿namespace ToothLess.Domain.Aggregates.PatientAggregate.Exceptions
{
    using ToothLess.Domain.Base;

    public class PatientDomainException : DomainException
    {
        public PatientDomainException(string message) : base(message)
        {
        }
    }
}
