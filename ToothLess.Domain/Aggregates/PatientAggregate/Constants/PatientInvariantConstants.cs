﻿namespace ToothLess.Domain.Aggregates.PatientAggregate.Constants
{
    public static class PatientInvariantConstants
    {
        public const int FirstNameMinLength = 2;
        public const int FirstNameMaxLength = 20;

        public const int LastNameMinLength = 2;
        public const int LastNameMaxLength = 20;
    }
}
