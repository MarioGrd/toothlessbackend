﻿namespace ToothLess.Domain.Aggregates.PatientAggregate
{
    using ToothLess.Domain.Base;

    public interface IPatientDomainRepository : IDomainRepository<Patient>
    {
    }
}
