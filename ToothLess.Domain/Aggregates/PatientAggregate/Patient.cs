﻿namespace ToothLess.Domain.Aggregates.PatientAggregate
{
    using System;
    using ToothLess.Domain.Aggregates.PatientAggregate.Constants;
    using ToothLess.Domain.Aggregates.PatientAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class Patient: Entity, IAggregateRoot
    {
        /// <summary>
        /// Patients name.
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Patients surname.
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Patient address.
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        /// Patient number.
        /// </summary>
        public string TelephoneNumber { get; private set; }

        /// <summary>
        /// Patients birth date.
        /// </summary>
        public DateTime BirthDate { get; private set; }

        /// <summary>
        /// Clinic identifier.
        /// </summary>
        public Guid ClinicId { get; private set; }

        /// <summary>
        /// Protected constructor for EF.
        /// </summary>
        protected Patient() { }

        /// <summary>
        /// Creates patient.
        /// </summary>
        /// <param name="firstName">Required first name.</param>
        /// <param name="lastName">Required last name.</param>
        /// <param name="address">Optional address.</param>
        /// <param name="telephoneNumber">Required telehone number.</param>
        /// <param name="birthDate">Required birth date.</param>
        /// <param name="clinicId">Required clinic identifier.</param>
        public Patient(string firstName, string lastName, string address, string telephoneNumber, DateTime birthDate, Guid clinicId)
        {
            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new PatientDomainException("First name must be provided.");
            }

            if (firstName.Length < PatientInvariantConstants.FirstNameMinLength || firstName.Length > PatientInvariantConstants.FirstNameMaxLength)
            {
                throw new PatientDomainException("Invalid first name length.");
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new PatientDomainException("Last name must be provided.");
            }

            if (lastName.Length < PatientInvariantConstants.LastNameMinLength || lastName.Length > PatientInvariantConstants.LastNameMaxLength)
            {
                throw new PatientDomainException("Invalid last name length.");
            }

            if (string.IsNullOrWhiteSpace(telephoneNumber))
            {
                throw new PatientDomainException("Number must be provided.");
            }

            if (birthDate == null)
            {
                throw new PatientDomainException("Birth date must be provided.");
            }

            if (clinicId == null)
            {
                throw new PatientDomainException("Clinic Id must be provided.");
            }

            this.Id = Guid.NewGuid();
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.TelephoneNumber = telephoneNumber;
            this.BirthDate = birthDate;
            this.ClinicId = clinicId;
        }

        public string GetFullName()
        {
            return $"{this.FirstName} {this.LastName}";
        }
    }
}
