﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate
{
    using ToothLess.Domain.Base;

    public interface IClinicDomainRepository : IDomainRepository<Clinic>
    {
    }
}
