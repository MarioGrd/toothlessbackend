﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions
{
    using ToothLess.Domain.Base;

    public class ClinicDomainException : DomainException
    {
        public ClinicDomainException(string message) : base(message)
        {
        }
    }
}
