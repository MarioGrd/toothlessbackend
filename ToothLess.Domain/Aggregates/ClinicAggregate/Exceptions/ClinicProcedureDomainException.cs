﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions
{
    using ToothLess.Domain.Base;

    public class ClinicProcedureDomainException : DomainException
    {
        public ClinicProcedureDomainException(string message) : base(message)
        {
        }
    }
}
