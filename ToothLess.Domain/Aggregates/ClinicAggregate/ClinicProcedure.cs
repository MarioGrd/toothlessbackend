﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate
{
    using System;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class ClinicProcedure : Entity
    {
        public string Name { get; private set; }
        public double Price { get; private set; }
        public int Minutes { get; private set; }
        public Guid ClinicId { get; private set; }

        public int ProcedureTypeId { get; private set; }

        /// <summary>
        /// Protected construtor for EF.
        /// </summary>
        protected ClinicProcedure() { }

        public ClinicProcedure(string name, double price, Guid clinicId, ProcedureType type)
        {
            if (clinicId == null)
            {
                throw new ClinicProcedureDomainException("Clinic Id not provided.");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ClinicProcedureDomainException("Procedure Name not provided.");
            }

            if (price < 0)
            {
                throw new ClinicProcedureDomainException("Procedure price cannot be lower than 0.");
            }

            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Price = price;
            this.Minutes = type.TimeInMinutes;
            this.ClinicId = clinicId;

            this.ProcedureTypeId = type.Id;
        }
    }
}
