﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class ProcedureType : Enumeration
    {
        public static ProcedureType Time_30 = new ProcedureType(1, nameof(Time_30).ToLowerInvariant(), 30);
        public static ProcedureType Time_60 = new ProcedureType(2, nameof(Time_60).ToLowerInvariant(), 60);
        public static ProcedureType Time_120 = new ProcedureType(3, nameof(Time_120).ToLowerInvariant(), 120);

        public int TimeInMinutes { get; private set; }

        protected ProcedureType()
        {
        }

        protected ProcedureType(int id, string name, int minutes)
            : base(id, name)
        {
            this.TimeInMinutes = minutes;
        }

        public static IEnumerable<ProcedureType> List() =>
            new[] { Time_30, Time_60, Time_120 };

        public static ProcedureType FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new ClinicProcedureDomainException($"Possible values for ProcedureType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static ProcedureType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new ClinicProcedureDomainException($"Possible values for ProcedureType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}
