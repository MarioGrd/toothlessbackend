﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate
{
    using System.Collections.Generic;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class WorkTime : ValueObject
    {
        public int StartMinute { get; private set; }
        public int StartHour { get; private set; }

        public int EndMinute { get; private set; }
        public int EndHour { get; private set; }


        protected WorkTime() { }

        public WorkTime(int startMinute, int startHour, int endMinute, int endHour)
        {
            if (startHour * 60 + startMinute > endHour * 60 + endMinute)
            {
                throw new ClinicDomainException("Start time must be greater than end time.");
            }

            if (startMinute < ClinicInvariantConstants.MinutesMin || startMinute > ClinicInvariantConstants.MinutesMax)
            {
                throw new ClinicDomainException($"Invalid {nameof(startMinute)} range.");
            }

            if (endMinute < ClinicInvariantConstants.MinutesMin || endMinute > ClinicInvariantConstants.MinutesMax)
            {
                throw new ClinicDomainException($"Invalid {nameof(endMinute)} range.");
            }

            if (startHour < ClinicInvariantConstants.HoursMin || startHour > ClinicInvariantConstants.HoursMax)
            {
                throw new ClinicDomainException($"Invalid {nameof(startHour)} range.");
            }

            if (endHour < ClinicInvariantConstants.HoursMin || endHour > ClinicInvariantConstants.HoursMax)
            {
                throw new ClinicDomainException($"Invalid {nameof(endHour)} range.");
            }

            this.StartMinute = startMinute;
            this.StartHour = startHour;
            this.EndMinute = endMinute;
            this.EndHour = endHour;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.StartMinute;
            yield return this.StartHour;
            yield return this.EndMinute;
            yield return this.EndHour;
        }
    }
}
