﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;
    using ToothLess.Domain.Aggregates.ClinicAggregate.DomainEvents;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions;
    using ToothLess.Domain.Base;
    using ToothLess.Doman.Helpers;

    public class Clinic : Entity, IAggregateRoot
    {
        /// <summary>
        /// Clinic name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Prodecures.
        /// </summary>
        private readonly List<ClinicProcedure> _procedures;

        /// <summary>
        /// Publicly exposed readonly procedures.
        /// </summary>
        public IReadOnlyCollection<ClinicProcedure> Procedures => _procedures;

        /// <summary>
        /// Value object of doctor.
        /// </summary>
        public ClinicDoctor Doctor { get; private set; }

        /// <summary>
        /// Working hours value object.
        /// </summary>
        public WorkTime WorkTime { get; private set; }

        /// <summary>
        /// Protected constructor for EF.
        /// </summary>
        protected Clinic()
        {
            this._procedures = new List<ClinicProcedure>();
        }

        public Clinic(string name, ClinicDoctor doctor, WorkTime workTime) : this()
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ClinicDomainException("Clinic name not provided.");
            }

            if (name.Length < ClinicInvariantConstants.ClinicNameMinLength || name.Length > ClinicInvariantConstants.ClinicNameMaxLength)
            {
                throw new ClinicDomainException("Invalid clinic name length.");
            }

            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Doctor = doctor;
            this.WorkTime = workTime;
        }

        public ClinicProcedure AddProcedure(string name, double price, int typeId)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ClinicProcedureDomainException("Procedure name not provided.");
            }

            if (name.Length < ClinicInvariantConstants.ProcedureNameMinLength || name.Length > ClinicInvariantConstants.ProcedureNameMaxLength)
            {
                throw new ClinicProcedureDomainException("Invalid procedure name length.");
            }

            var procedure = new ClinicProcedure(
                name: name, 
                price: price, 
                clinicId: this.Id, 
                type: ProcedureType.From(typeId));

            _procedures.Add(procedure);

            return procedure;
        }

        public void RemoveProcedure(Guid procedureId)
        {
            var procedure = _procedures.FirstOrDefault(p => p.Id == procedureId);

            if (procedure == null)
            {
                return;
            }

            _procedures.Remove(procedure);
        }

        public void AddAppointmentValidatedDomainEvent(DateTime startTime, Guid patientId, Guid procedureId, Guid appointmentId)
        {
            var procedure = this._procedures.SingleOrDefault(p => p.Id == procedureId);

            if (procedure == null)
            {
                throw new ClinicDomainException("Procedure not found.");
            }

            var endTime = startTime.AddMinutes(ProcedureType.From(procedure.ProcedureTypeId).TimeInMinutes);

            if (DateTimeHelper.IsWeekend(startTime) || DateTimeHelper.IsWeekend(endTime))
            {
                throw new ClinicDomainException("Weekend is selected.");
            }

            var requestStartTme = DateTimeHelper.TotalMinutes(startTime);
            var requestEndTime =  DateTimeHelper.TotalMinutes(endTime);

            var clinicStartTime = this.WorkTime.StartHour * 60 + this.WorkTime.StartMinute;
            var clinicEndTime = this.WorkTime.EndHour * 60 + this.WorkTime.EndMinute;

            if (requestStartTme > requestEndTime)
            {
                throw new ClinicDomainException("Requested start time is greater than requested end time.");
            }

            if (requestStartTme < clinicStartTime || requestStartTme > clinicEndTime)
            {
                throw new ClinicDomainException("Requested start time is not in clinic working hour range.");
            }

            if (requestEndTime < clinicStartTime || requestEndTime > clinicEndTime)
            {
                throw new ClinicDomainException("Requested end time is not in clinic working hour range.");
            }

            var appointmentValidatedDomainEvent = 
                new AppointmentValidatedDomainEvent(
                    appointmentId: appointmentId,
                    clinicId: this.Id, 
                    patientId: patientId,
                    procedureId: procedureId,
                    startTime: startTime, 
                    endTime: endTime);

            this.AddDomainEvent(appointmentValidatedDomainEvent);
        }
    }
}
