﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate.Constants
{
    public static class ClinicInvariantConstants
    {
        #region Clinic

        public const int ClinicNameMinLength = 2;
        public const int ClinicNameMaxLength = 50;

        #endregion

        #region Procedure

        public const int ProcedureNameMinLength = 2;
        public const int ProcedureNameMaxLength = 50;

        #endregion

        #region WorkTime

        public const int MinutesMin = 0;
        public const int MinutesMax = 60;
        public const int HoursMin = 0;
        public const int HoursMax = 24;

        #endregion

        #region ClinicDoctor

        public const int DoctorNameMinLength = 2;
        public const int DoctorNameMaxLength = 20;

        #endregion
    }
}
