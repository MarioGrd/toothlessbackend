﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate
{
    using System.Collections.Generic;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Exceptions;
    using ToothLess.Domain.Base;

    public class ClinicDoctor : ValueObject
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        protected ClinicDoctor() { }

        public ClinicDoctor(string firstName, string lastName)
        {
            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new ClinicDomainException("FirstName is required.");
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ClinicDomainException("LastName is required.");
            }

            this.FirstName = firstName;
            this.LastName = lastName;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.FirstName;
            yield return this.LastName;
        }
    }
}
