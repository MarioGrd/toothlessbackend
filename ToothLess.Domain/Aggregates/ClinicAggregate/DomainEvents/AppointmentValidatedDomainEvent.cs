﻿namespace ToothLess.Domain.Aggregates.ClinicAggregate.DomainEvents
{
    using MediatR;
    using System;

    public class AppointmentValidatedDomainEvent : INotification
    {
        public Guid AppointmentId { get; }
        public Guid ClinicId { get; }
        public Guid PatientId { get; }
        public Guid ProcedureId { get; }
        public DateTime StartTime { get; }
        public DateTime EndTime { get; }

        public AppointmentValidatedDomainEvent(Guid clinicId, Guid patientId, DateTime startTime, DateTime endTime, Guid procedureId, Guid appointmentId)
        {
            this.AppointmentId = appointmentId;
            this.ClinicId = clinicId;
            this.PatientId = patientId;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.ProcedureId = procedureId;
        }
    }
}
