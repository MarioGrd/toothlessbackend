﻿namespace ToothLess.Infrastructure.Repositories.QueryRepository
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Database;
    using ToothLess.Infrastructure.Base.Helpers;

    public interface IQueryRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Returns <see cref="IQueryable{TEntity}"/> with no tracking.
        /// </summary>
        IQueryable<TEntity> Query { get; }
    }

    public class QueryRepository<TEntity> : IQueryRepository<TEntity> where TEntity : Entity
    {
        private readonly DbSet<TEntity> _dbSet;

        /// <summary>
        /// Returns <see cref="IQueryable{TEntity}"/>
        /// </summary>
        public IQueryable<TEntity> Query
        {
            get
            {
                return this._dbSet.AsNoTracking();
            }
        }

        public QueryRepository(ToothLessContext dbContext)
        {
            ArgumentChecker.CheckNotNull(new { dbContext });

            this._dbSet = dbContext.Set<TEntity>();
        }
    }
}
