﻿namespace ToothLess.Infrastructure.Repositories.DomainRepository
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Database;
    using ToothLess.Infrastructure.Base.Helpers;

    public class PatientDomainRepository : IPatientDomainRepository
    {
        public IUnitOfWork UnitOfWork => this._context;

        private readonly ToothLessContext _context;
        private readonly DbSet<Patient> _patients;

        public PatientDomainRepository(ToothLessContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._patients = context.Set<Patient>();
        }

        public void Add(Patient patient)
        {
            this._patients.Add(patient);
        }

        public void Delete(Patient patient)
        {
            this._patients.Remove(patient);
        }

        public async Task<Patient> GetByIdAsync(Guid id)
        {
            var patient =
                await this._patients.SingleOrDefaultAsync(p => p.Id == id);

            if (patient == null)
            {
                throw new ArgumentNullException("Patient not found.");
            }

            return patient;
        }

        public void Update(Patient patient)
        {
            this._patients.Update(patient);
        }
    }
}
