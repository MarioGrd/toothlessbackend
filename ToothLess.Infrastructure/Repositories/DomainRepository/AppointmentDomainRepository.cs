﻿namespace ToothLess.Infrastructure.Repositories.DomainRepository
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Database;
    using ToothLess.Infrastructure.Base.Helpers;

    public class AppointmentDomainRepository : IAppointmentDomainRepository
    {
        public IUnitOfWork UnitOfWork => this._context;

        private readonly ToothLessContext _context;
        private readonly DbSet<Appointment> _appointments;

        public AppointmentDomainRepository(ToothLessContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._appointments = context.Set<Appointment>();
        }

        public void Add(Appointment appointment)
        {
            this._appointments.Add(appointment);
        }

        public void Delete(Appointment appointment)
        {
            this._appointments.Remove(appointment);
        }

        public async Task<Appointment> GetByIdAsync(Guid id)
        {
            var appointment =
                await this._appointments.SingleOrDefaultAsync(p => p.Id == id);

            if (appointment == null)
            {
                throw new ArgumentNullException("Appointment not found");
            }

            return appointment;
        }

        public void Update(Appointment appointment)
        {
            this._appointments.Update(appointment);
        }
    }
}
