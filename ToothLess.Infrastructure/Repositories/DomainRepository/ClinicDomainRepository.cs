﻿namespace ToothLess.Infrastructure.Repositories.DomainRepository
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Database;
    using ToothLess.Infrastructure.Base.Helpers;

    public class ClinicDomainRepository : IClinicDomainRepository
    {
        public IUnitOfWork UnitOfWork => this._context;

        private readonly ToothLessContext _context;
        private readonly DbSet<Clinic> _clinics;

        public ClinicDomainRepository(ToothLessContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._clinics = this._context.Set<Clinic>();
        }

        public void Add(Clinic clinic)
        {
            this._clinics.Add(clinic);
        }

        public void Delete(Clinic clinic)
        {
            this._clinics.Remove(clinic);
        }

        public async Task<Clinic> GetByIdAsync(Guid id)
        {
            var clinic = 
                await this._clinics
                    .Include(p => p.Procedures)
                    .Include(p => p.Doctor)
                    .Include(p => p.WorkTime)
                    .SingleOrDefaultAsync(c => c.Id == id);

            if (clinic == null)
            {
                throw new ArgumentNullException("Clinic not found.");
            }

            return clinic;
        }

        public void Update(Clinic clinic)
        {
            this._clinics.Update(clinic);
        }
    }
}
