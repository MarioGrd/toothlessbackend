﻿namespace ToothLess.Infrastructure.Base.Database
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Threading;
    using System.Threading.Tasks;
    using ToothLess.Domain.Base;
    using ToothLess.Infrastructure.Base.Extenders;
    using ToothLess.Infrastructure.Base.Helpers;

    public class ToothLessContext : DbContext, IUnitOfWork
        {

            private ToothLessContext(DbContextOptions<ToothLessContext> options) : base(options) { }

            public ToothLessContext(DbContextOptions<ToothLessContext> options, IMediator mediator)
                : base(options)
            {
                ArgumentChecker.CheckNotNull(new { mediator });
                this._mediator = mediator;
            }

            private readonly IMediator _mediator;

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.ApplyEntityTypeConfigurations();

            }

            public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
            {
                // Dispatch Domain Events collection. 
                // Choices:
                // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
                // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime
                // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
                // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
                await _mediator.DispatchDomainEventsAsync(this);

                // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
                // performed throught the DbContext will be commited
                var result = await base.SaveChangesAsync();

                return true;
            }

        }
}
