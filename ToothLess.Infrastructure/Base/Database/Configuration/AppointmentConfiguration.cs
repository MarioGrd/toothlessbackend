﻿namespace ToothLess.Infrastructure.Base.Database.Configuration
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ToothLess.Domain.Aggregates.AppointmentAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate;

    public class AppointmentConfiguration : IEntityTypeConfiguration<Appointment>
    {
        private const string TableName = "Appointments";

        public void Configure(EntityTypeBuilder<Appointment> builder)
        {
            builder.ToTable(TableName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.StartTime)
                .IsRequired();

            builder.Property(p => p.EndTime)
                .IsRequired();

            builder.HasOne<Clinic>()
                .WithMany()
                .HasForeignKey(p => p.ClinicId)
                .IsRequired();

            builder.HasOne<Patient>()
                .WithMany()
                .HasForeignKey(p => p.PatientId)
                .IsRequired();

            builder.Property(p => p.AppointmentStatusId)
                .IsRequired();

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
