﻿namespace ToothLess.Infrastructure.Base.Database.Configuration
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;

    public class ClinicProcedureConfiguration : IEntityTypeConfiguration<ClinicProcedure>
    {
        private const string TableName = "ClinicProcedures";

        public void Configure(EntityTypeBuilder<ClinicProcedure> builder)
        {
            builder.ToTable(TableName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(ClinicInvariantConstants.ClinicNameMaxLength);

            builder.Property(p => p.Minutes)
                .IsRequired();

            builder.Property(p => p.ProcedureTypeId)
                .IsRequired();

            builder
                .HasOne<Clinic>()
                .WithMany(p => p.Procedures)
                .HasForeignKey(p => p.ClinicId)
                .IsRequired();

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
