﻿namespace ToothLess.Infrastructure.Base.Database.Configuration
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ToothLess.Domain.Aggregates.ClinicAggregate;
    using ToothLess.Domain.Aggregates.ClinicAggregate.Constants;

    public class ClinicConfiguration : IEntityTypeConfiguration<Clinic>
    {
        private const string TableName = "Clinics";

        public void Configure(EntityTypeBuilder<Clinic> builder)
        {
            builder.ToTable(TableName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(ClinicInvariantConstants.ClinicNameMaxLength);

            builder.OwnsOne(p => p.Doctor);

            builder.OwnsOne(p => p.WorkTime);

            var procedures = builder.Metadata.FindNavigation(nameof(Clinic.Procedures));
            procedures.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
