﻿namespace ToothLess.Infrastructure.Base.Database.Configuration
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ToothLess.Domain.Aggregates.PatientAggregate;
    using ToothLess.Domain.Aggregates.PatientAggregate.Constants;

    public class PatientConfiguration : IEntityTypeConfiguration<Patient>
    {
        private const string TableName = "Patients";

        public void Configure(EntityTypeBuilder<Patient> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(p => p.Id);

            builder.Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(PatientInvariantConstants.FirstNameMaxLength);

            builder.Property(p => p.LastName)
                .IsRequired()
                .HasMaxLength(PatientInvariantConstants.LastNameMaxLength);

            builder.Property(p => p.BirthDate)
                .IsRequired();

            builder.Property(p => p.TelephoneNumber)
                .IsRequired();

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
