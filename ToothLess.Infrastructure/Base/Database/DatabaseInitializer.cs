﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToothLess.Domain.Aggregates.AppointmentAggregate;
using ToothLess.Domain.Aggregates.ClinicAggregate;
using ToothLess.Domain.Aggregates.ClinicAggregate.Enumerations;
using ToothLess.Domain.Aggregates.PatientAggregate;
using ToothLess.Doman.Helpers;

namespace ToothLess.Infrastructure.Base.Database
{
    public sealed class DatabaseInitializer
    {
        public static void Initialize(ToothLessContext context)
        {
            context.Database.Migrate();

            if (context.Set<Clinic>().Any())
            {
                return;
            }

            InitializeClinic(context);
            InitializePatients(context);
            InitializeAppointments(context);
        }

        private static void InitializeClinic(ToothLessContext context)
        {
            var clinic = new Clinic(
                name: "ToothLessClinic",
                doctor: new ClinicDoctor("Will", "Smith"),
                workTime: new WorkTime(0, 8, 0, 16));

            context.Set<Clinic>().Add(clinic);

            context.SaveChanges();

            for (var i = 0; i < 10; i++)
            {
                var random = new Random();

                int typeId = random.Next(1, 3);

                var procedureType = ProcedureType.From(typeId);

                clinic.AddProcedure(
                    name: $"Procedure{i}",
                    price: i * 10,
                    typeId: procedureType.Id);
            }

            context.SaveChanges();
        }


        private static void InitializePatients(ToothLessContext context)
        {
            var clinic = context.Set<Clinic>().First();

            for (var i = 0; i < 10; i++)
            {
                var patient = new Patient(
                    firstName: $"Firstname{i}",
                    lastName: $"Lastname{i}",
                    address: $"Address{i}",
                    telephoneNumber: $"Number{i}",
                    birthDate: DateTime.UtcNow,
                    clinicId: clinic.Id);

                context.Set<Patient>().Add(patient);
            }

            context.SaveChanges();
        }

        private static void InitializeAppointments(ToothLessContext context)
        {
            List<Patient> patients = context.Set<Patient>().Skip(0).Take(5).ToList();
            var clinic = context.Set<Clinic>().Include(p => p.Procedures).First();

            var procedure_30 = clinic.Procedures.Where(p => p.ProcedureTypeId == ProcedureType.Time_30.Id).First();
            var procedure_60 = clinic.Procedures.Where(p => p.ProcedureTypeId == ProcedureType.Time_60.Id).First();


            var now = DateTime.Now;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);

            var days = DateTimeHelper.DateRange(startOfMonth, now);

            foreach (var day in days)
            {
                if (DateTimeHelper.IsWeekend(day))
                {
                    continue;
                }

                var clinicStart = new DateTime(day.Year, day.Month, day.Day, clinic.WorkTime.StartHour, clinic.WorkTime.StartMinute, 0);
                var clinicEnd = new DateTime(day.Year, day.Month, day.Day, clinic.WorkTime.EndHour, clinic.WorkTime.EndMinute, 0);

                for (var i = 0; i < patients.Count(); i++)
                {
                    ClinicProcedure procedure = null;

                    if (i / 2 == 0)
                    {
                        procedure = procedure_30;
                    }
                    else
                    {
                        procedure = procedure_60;
                    }


                    var startMinutes = ProcedureType.From(procedure.ProcedureTypeId).TimeInMinutes * (i + 1);
                    var endMinutes = ProcedureType.From(procedure.ProcedureTypeId).TimeInMinutes * (i + 2);

                    var start = clinicStart.AddMinutes(startMinutes);
                    var end = clinicStart.AddMinutes(endMinutes);



                    if (start > clinicStart && start < clinicEnd && end > clinicStart && end < clinicEnd)
                    {
                        var appointment = new Appointment(
                            procedureId: procedure.Id,
                            appointmentId: Guid.NewGuid(),
                            startTime: start,
                            endTime: end,
                            clinicId: clinic.Id,
                            patientId: patients[i].Id);

                        if (i / 2 == 0)
                        {
                            appointment.MarkAppointmentCompleted();
                        } else
                        {
                            appointment.MarkAppointmentMissed();
                        }

                        context.Set<Appointment>().Add(appointment);
                    }
                }
            }
            context.SaveChanges();
        }

    }
}
