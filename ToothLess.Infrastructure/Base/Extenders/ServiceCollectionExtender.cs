﻿namespace ToothLess.Infrastructure.Base.Extenders
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using System.Reflection;
    using ToothLess.Infrastructure.Base.Database;

    public static class ServiceCollectionExtender
    {
        public static IServiceCollection AddToothLessDbContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ToothLessContext>(options =>
            {
                options.UseSqlServer(
                    connectionString,
                    config => config.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
            });

            return services;
        }
    }
}
