﻿namespace ToothLess.Infrastructure.Services.Implementation
{
    using System;

    public interface ITimeService
    {
        DateTime UtcNow { get; }

        DateTime Now { get; }
    }

    public class TimeService : ITimeService
    {
        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime Now => DateTime.Now;
    }
}
